import {
    CART_LOADED, CART_ITEM_ADDED, CART_SHOW, CART_HIDE,
} from '../actions/cart';

function cartReducer(state = {items: {}, cartShowing: false}, action) {

    switch (action.type) {
        case CART_LOADED:
            return {
                ...state,
                id: action.cartId,
                items: action.cartItems,
            }
        case CART_ITEM_ADDED:
            return {
                ...state,
                items: {
                    ...state.items,
                    ...action.cartItems
                }
            }
        case CART_SHOW:
            return {
                ...state,
                cartShowing: true
            }
        case CART_HIDE:
            return {
                ...state,
                cartShowing: false
            }
        default:
            return state;
    }

}

export default cartReducer;
