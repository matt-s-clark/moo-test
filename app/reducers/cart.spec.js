import * as actions from '../actions/cart';
import cartReducer from './cart';

describe('cartReducer', () => {

    it('should return an empty initial state', () => {
        expect(cartReducer(undefined, {})).toEqual({cartShowing: false, items: {}});
    });

    it('should store cart id and cartItems when cart is loaded', () => {
        const action = {
            type: actions.CART_LOADED,
            cartId: 123,
            cartItems: {}
        };

        expect(cartReducer(undefined, action)).toEqual({
            cartShowing: false,
            id: 123,
            items: {},
        });
    });

    it('should store additional cartItems when a new item is added', () => {
        const action = {
            type: actions.CART_ITEM_ADDED,
            cartItems: {
                '3': {itemId: 9, quantity: 1}
            }
        };

        const cart = {
            items: {
                '1': {itemId: 3, quantity: 1},
                '2': {itemId: 6, quantity: 1}
            },
        };

        expect(cartReducer(cart, action)).toEqual({
            items: {
                '1': {itemId: 3, quantity: 1},
                '2': {itemId: 6, quantity: 1},
                '3': {itemId: 9, quantity: 1}
            },
        });
    });

    it('should hide the cart', () => {
        const action = {
            type: actions.CART_HIDE
        };

        const cart = {
            items: {},
            cartShowing: true
        };
        expect(cartReducer(cart, action)).toEqual({cartShowing: false, items: {}});
    });

    it('should show the cart', () => {
        const action = {
            type: actions.CART_SHOW
        };

        const cart = {
            items: {},
            cartShowing: false
        };
        expect(cartReducer(cart, action)).toEqual({cartShowing: true, items: {}});
    });
});
