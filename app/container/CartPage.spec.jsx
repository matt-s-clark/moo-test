import React from 'react';
import {shallow, mount} from 'enzyme';

import {CartPage, mapStateToProps, composeCartItems, sumCartItems} from './CartPage';
import {ProductList, Cart} from '../components';

describe('<CartPage />', () => {

    const cart = [];
    const products = [];

    const validProducts = [
        {name: 'A', id: 1, price: '10.45'},
        {name: 'B', id: 2, price: '150.00'},
        {name: 'C', id: 3, price: '299.99'}
    ]

    const validCart = {
        id: 100,
        items: {
            '1': {itemId: 1, quantity: 1},
            '2': {itemId: 3, quantity: 1},
        }
    }

    describe('rendering', () => {
        it('should start loading the cart when the cart page gets created', () => {
            // Given
            const loadCart = jest.fn();

            // When
            const props = {cart, products, loadCart};
            const render = shallow(<CartPage {...props} />);

            // Then
            expect(loadCart).toHaveBeenCalled();
        });

        it('should display a loading message when loading the cart', () => {
            // Given
            const loadCart = jest.fn();
            const isLoading = true;

            // When
            const props = {cart, products, isLoading, loadCart};
            const render = shallow(<CartPage {...props} />);

            // Then
            expect(render.find('div').text()).toContain('Loading your cart');
        });

        it('should call the addToCart function when the ProductList Add button is clicked', () => {
            // Given
            const addToCart = jest.fn();
            const loadCart = jest.fn();
            const props = {
                cart,
                products: [{name: 'A', id: 1, price: '10.45'}],
                addToCart,
                loadCart,
                isShowing: true
            };

            // When
            const render = mount(<CartPage {...props} />);
            const addButton = render.find(ProductList).first().find('button').simulate('click');

            // Then
            expect(addToCart).toBeCalled();
        });

        it('should call the increment function when the Cart increment button is clicked', () => {
            // Given
            const addToCart = jest.fn();
            const loadCart = jest.fn();
            const increment = jest.fn();
            const showCart = jest.fn();
            const props = {
                cart: {
                    id: 100,
                    items: {
                        '1': {itemId: 1, quantity: 1}
                    }
                },
                products: [{name: 'A', id: 1, price: '10.45'}],
                addToCart,
                loadCart,
                increment,
                showCart,
                cartShowing: true
            };

            // When
            const render = mount(<CartPage {...props} />);
            render.find('button').at(1).simulate('click');

            // Then
            expect(increment).toBeCalled();
        });

        it('should call the decrement function when the Cart decrement button is clicked', () => {
            // Given
            const addToCart = jest.fn();
            const loadCart = jest.fn();
            const decrement = jest.fn();
            const increment = jest.fn();
            const showCart = jest.fn();
            const props = {
                cart: {
                    id: 100,
                    items: {
                        '1': {itemId: 1, quantity: 1}
                    }
                },
                products: [{name: 'A', id: 1, price: '10.45'}],
                addToCart,
                loadCart,
                increment,
                decrement,
                showCart,
                cartShowing: true
            };

            // When
            const render = mount(<CartPage {...props} />);
            render.find('button').at(2).simulate('click');

            // Then
            expect(decrement).toBeCalled();
        });

        it('should call the showCart function when the CartIcon is clicked', () => {
            // Given
            const addToCart = jest.fn();
            const loadCart = jest.fn();
            const decrement = jest.fn();
            const increment = jest.fn();
            const showCart = jest.fn();
            const props = {
                cart: {
                    id: 100,
                    items: {
                        '1': {itemId: 1, quantity: 1}
                    }
                },
                products: [{name: 'A', id: 1, price: '10.45'}],
                addToCart,
                loadCart,
                increment,
                decrement,
                showCart,
                cartShowing: false
            };

            // When
            const render = mount(<CartPage {...props} />);
            render.find('button').first().simulate('click');

            // Then
            expect(showCart).toBeCalled();
        });
    });

    describe('helper function composeCartItems', () => {
        it('should compose a result of zero when there are no items in the cart', () => {
            expect(sumCartItems({})).toEqual(0);
        });
        it('should compose a result of two when there are two items in the cart', () => {
            expect(sumCartItems(validCart)).toEqual(2);
        });
    });

    describe('helper function composeCartItems', () => {
        it('should compose empty cartItems when there are no products', () => {
            expect(composeCartItems([], [])).toEqual([]);
        });

        it('should compose empty cartItems when there is nothing in the cart', () => {
            expect(composeCartItems([], validProducts)).toEqual([]);
        });

        it('should compose cartItems when there are items in the cart', () => {
            expect(composeCartItems(validCart, validProducts)).toEqual([
                {"productId": 1, "productName": "A", "quantity": 1}, {"productId": 3, "productName": "C", "quantity": 1}
            ]);
        });
    });

    describe('redux functions', () => {
        it('should create empty state to props mapping', () => {
            expect(mapStateToProps({
                cart, products
            })).toEqual({cart: [], isLoading: true, cartShowing: false, products: []});
        });

        it('should create populated state to props mapping', () => {
            expect(mapStateToProps({
                cart: validCart,
                products: validProducts
            })).toEqual(
                {
                    "cart": {
                        "id": 100,
                        "items": {
                            "1": {"itemId": 1, "quantity": 1},
                            "2": {"itemId": 3, "quantity": 1}
                        }
                    },
                    "cartShowing": false,
                    "isLoading": false,
                    "products": [
                        {"id": 1, "name": "A", "price": "10.45"},
                        {"id": 2, "name": "B", "price": "150.00"},
                        {"id": 3, "name": "C", "price": "299.99"}
                    ]
                });
        });
    });
});
