import React, {Component} from 'react';
import {connect} from 'react-redux';

import {ProductList, Cart, CartIcon} from '../components';

import * as cartActions from '../actions/cart';

export class CartPage extends Component {

    constructor(props) {
        super(props);
        props.loadCart();
    }

    render() {
        const {
            isLoading, cartShowing, products, cart,
            addToCart, increment, decrement, showCart
        } = this.props;

        if (isLoading) {
            return <div> Loading your cart </div>;
        }

        return (
            <div className="container">
                <header className="navbar navbar-light bg-light row">
                    <span className="navbar-brand col-10">The Shop of Wonderous Marvels</span>
                    <CartIcon cartCounter={sumCartItems(cart)} onClick={() => showCart(!cartShowing)} />
                </header>
                <main className="container row">
                    {cartShowing
                        ? (
                            <section className="col">
                                <Cart
                                    cartItems={composeCartItems(cart, products)}
                                    increment={(productId) => increment(productId)}
                                    decrement={(productId) => decrement(productId)}
                                />
                            </section>
                        )
                        : (
                            <section className="col">
                                <ProductList products={products} addToCart={(id) => addToCart(id, cart.id)} />
                            </section>

                        )}
                </main>
            </div>
        )
    }
}

export function composeCartItems(cart, products) {

    if (!products || !products.length) return [];
    if (!cart || !cart.items) return [];

    return Object.keys(cart.items).map(cartItemId => {
        const productId = cart.items[cartItemId].itemId;

        return {
            productId: productId,
            productName: products
                .find(product => product.id === productId)
                .name,
            quantity: cart.items[cartItemId].quantity
        };
    });
}

export function sumCartItems(cart) {

    if (!cart || !cart.items) return 0;

    let sum = 0;
    Object.keys(cart.items).forEach((id) => sum += cart.items[id].quantity);

    return sum;
}

export function mapStateToProps({cart, products}) {
    return {
        isLoading: !cart.id,
        cartShowing: cart.cartShowing || false,
        products,
        cart,
    }
}

export default connect(mapStateToProps, cartActions)(CartPage);
