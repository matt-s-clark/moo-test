import React from 'react';
import {shallow} from 'enzyme';
import CartIcon from './CartIcon';

describe('<CartIcon />', () => {

    it('should render a shopping cart icon button', () => {
        // Given
        const onClick = jest.fn();
        const props = {
            cartCounter: 0,
            onClick
        }

        // When
        const render = shallow(<CartIcon {...props} />);
        render.find('button').first().simulate('click');

        // Then
        expect(onClick).toBeCalled();
    });
});