import React from 'react';

const Cart = ({cartItems, increment, decrement}) => (
    <div className="mr-4 mt-4">
        <h5 className="mb-3">Cart</h5>
        <ul className="list-group">
            {
                cartItems.map(item => (
                    <li key={item.productName} className="list-group-item d-flex justify-content-between">
                        <span>{item.productName}</span>
                        <span>{item.quantity}</span>
                        <div>
                            <button className="btn btn-outline-primary btn-sm" type="button" onClick={() => increment(item.productId)}>Add</button>
                            <button className="btn btn-outline-primary btn-sm" type="button" onClick={() => decrement(item.productId)}>Remove</button>
                        </div>
                    </li>
                ))
            }
        </ul>
    </div>
);

export default Cart;
