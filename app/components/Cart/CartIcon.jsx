import React from 'react';

const CartIcon = ({cartCounter, onClick}) => (
    <button className="btn btn-light" onClick={(event) => onClick(event)}>
        <svg style={{width: '18px'}} viewBox="0 0 48 48" aria-hidden="true">
            <path d="M10.437 38.527a3.5 3.5 0 1 0 3.5-3.5 3.5 3.5 0 0 0-3.5 3.5zm5 0a1.5 1.5 0 1 1-1.5-1.5 1.5 1.5 0 0 1 1.5 1.5zm16.255 0a3.5 3.5 0 1 0 3.5-3.5 3.5 3.5 0 0 0-3.5 3.5zm5 0a1.5 1.5 0 1 1-1.5-1.5 1.5 1.5 0 0 1 1.5 1.5zM10.1 32.655h28.852l3.365-20.87H8.762L7.339 2.96H0v2h5.636zm29.87-18.87l-2.721 16.87H11.8l-2.716-16.87z"></path>
        </svg>
        <span className="ml-1">Cart ({cartCounter})</span>
    </button>
);

export default CartIcon;