import React from 'react';
import {shallow} from 'enzyme';
import Cart from './Cart';


describe('<Cart />', () => {

    it('should render a shopping cart', () => {
        // Given
        const cartItems = [
            {productName: 'A', quantity: 1},
            {productName: 'B', quantity: 2}
        ];

        // When
        const render = shallow(<Cart cartItems={cartItems} />);
        const listItems = render.find('li');

        // Then
        expect(listItems).toHaveLength(2);
        expect(listItems.first().find('span').first().text()).toContain('A');
        expect(listItems.at(1).find('span').first().text()).toContain('B');
    });

    it('should call increment function when increment button pressed', () => {
        // Given
        const cartItems = [
            {productId: 11, productName: 'A', quantity: 1},
            {productId: 22, productName: 'B', quantity: 2}
        ];
        const increment = jest.fn();

        // When
        const props = {cartItems, increment};
        const render = shallow(<Cart {...props} />);
        render.find('button').first().simulate('click');

        // Then
        expect(increment).toHaveBeenCalled();
    });

    it('should call decrement function when decrement button pressed', () => {
        // Given
        const cartItems = [
            {productId: 11, productName: 'A', quantity: 1},
            {productId: 22, productName: 'B', quantity: 2}
        ];
        const decrement = jest.fn();

        // When
        const props = {cartItems, decrement};
        const render = shallow(<Cart {...props} />);
        render.find('button').at(1).simulate('click');

        // Then
        expect(decrement).toHaveBeenCalled();
    });
});
