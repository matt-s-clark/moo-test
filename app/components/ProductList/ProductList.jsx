import React from 'react';

const ProductList = ({products, addToCart}) => (
    <div className="mr-4 mt-4">
        <h5 className="mb-3">Products</h5>
        <ul className="list-group">
            {
                products.map(product => {
                    return (
                        <li className="list-group-item d-flex justify-content-between" key={product.name}>
                            <span>{product.name}</span>
                            <button className="btn btn-outline-primary btn-sm" type="button" onClick={() => addToCart(product.id)}>Add</button>
                        </li>
                    )
                })
            }
        </ul>
    </div>
);

export default ProductList;
