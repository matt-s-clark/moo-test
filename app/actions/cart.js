export const CART_LOADING = 'CART_LOADING';
export const CART_LOADED = 'CART_LOADED';
export const CART_ITEM_ADDING = 'CART_ITEM_ADDING';
export const CART_ITEM_ADDED = 'CART_ITEM_ADDED';
export const CART_SHOW = 'CART_SHOW';
export const CART_HIDE = 'CART_HIDE';

export function loadCart() {
    return async (dispatch, _, config) => {
        dispatch({type: CART_LOADING});

        const response = await fetch(config.cartApi, {method: 'POST', body: JSON.stringify({})});
        const cart = await response.json();

        dispatch({
            type: CART_LOADED,
            ...cart,
        })
    };
}

export function addToCart(productId, cartId) {
    return async (dispatch, _, config) => {
        dispatch({type: CART_ITEM_ADDING});

        const response = await fetch(
            config.cartApi + `/${cartId}/item/${productId}`,
            {
                method: 'POST',
                body: JSON.stringify({quantity: 1})
            }
        );
        const cart = await response.json();

        dispatch({
            type: CART_ITEM_ADDED,
            ...cart,
        })
    };
}

export function showCart(show) {
    return (dispatch) => {
        if (show === true) {
            dispatch({type: CART_SHOW})
        } else {
            dispatch({type: CART_HIDE})
        }
    }
}

export function increment(productId) {
    return (dispatch) => {
        console.log('TODO - Implement increment')
    }
}

export function decrement(productId) {
    return (dispatch) => {
        console.log('TODO - Implement decrement')
    }
}