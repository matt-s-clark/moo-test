import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';
import * as actions from './cart';


const config = {'cartApi': '/cart'};
const middlewares = [thunk.withExtraArgument(config)];
const mockStore = configureMockStore(middlewares);

describe('cart actions', () => {

    describe('loadCart', () => {

        it('creates a cart when CART_LOADED is dispatched', async () => {
            // Given
            fetchMock.postOnce('/cart', {body: {cartId: '', items: []}});

            // When
            const store = mockStore({});
            await store.dispatch(actions.loadCart());

            // Then
            expect(store.getActions()).toEqual([
                {type: actions.CART_LOADING},
                {type: actions.CART_LOADED, cartId: '', items: []},
            ]);
        });

    });

    describe('addToCart', () => {

        it('adds an item to the cart when CART_ITEM_ADDED is dispatched', async () => {
            // Given
            fetchMock.postOnce('/cart/100/item/1', {
                body: {
                    cartId: 100,
                    "cartItems":
                    {
                        "1":
                        {
                            "itemId": 1,
                            "quantity": 1
                        }
                    }
                }
            });

            // When
            const store = mockStore({});
            await store.dispatch(actions.addToCart(1, 100));

            // Then
            expect(store.getActions()).toEqual([
                {type: actions.CART_ITEM_ADDING},
                {
                    "cartId": 100,
                    "cartItems": {
                        "1": {
                            "itemId": 1,
                            "quantity": 1
                        }
                    },
                    "type": "CART_ITEM_ADDED"
                },
            ]);
        });
    });

    describe('showCart', () => {

        it('sets cartShowing to true when CART_SHOW is dispatched', () => {
            // When
            const store = mockStore({});
            store.dispatch(actions.showCart(true));

            // Then
            expect(store.getActions()).toEqual([{"type": "CART_SHOW"}]);
        });

        it('sets cartShowing to false when CART_HIDE is dispatched', () => {
            // When
            const store = mockStore({});
            store.dispatch(actions.showCart(false));

            // Then
            expect(store.getActions()).toEqual([{"type": "CART_HIDE"}]);
        });
    });

    describe('TODO', () => {
        it('implement increment and decrement functions', () => {

            // Placeholder tests for when increment and decrement are implemented
            const store = mockStore({});
            store.dispatch(actions.increment(0));
            expect(store.getActions()).toEqual([]);

            store.dispatch(actions.decrement(0));
            expect(store.getActions()).toEqual([]);
        });
    });
});
